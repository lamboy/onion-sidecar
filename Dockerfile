FROM debian:bullseye-slim

LABEL maintainer="hallo@lamboy.net"

RUN apt-get update && apt-get install -y tor

VOLUME /var/lib/tor/

COPY docker-entrypoint.sh /usr/local/bin/

USER debian-tor

ENTRYPOINT ["docker-entrypoint.sh"]

CMD ["tor", "-f", "/var/lib/tor/.torrc"]
