# Onion Sidecar

Create a `docker-compose.yml` file with the following content:

```yaml
version: "3"

services:
  web:
    image: nginx:latest
    expose:
      - "80"
  onion:
    image: registry.gitlab.com/lamboy/onion-sidecar
    environment:
      ONION_NAME: "sample"
      ONION_PORT: "80"
      ONION_TARGET: "web:80"
```

Run the services:

    $ docker-compose up -d

Retrieve the onion service address:

    $ docker-compose exec onion cat /var/lib/tor/sample/hostname
