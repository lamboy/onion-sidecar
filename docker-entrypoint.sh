#!/bin/bash
set -e

CONFIG_FILE=/var/lib/tor/.torrc

if [ "$1" = 'tor' ]; then
  echo "HiddenServiceDir /var/lib/tor/$ONION_NAME/" > $CONFIG_FILE
  echo "HiddenServicePort $ONION_PORT $ONION_TARGET" >> $CONFIG_FILE
fi

exec "$@"
